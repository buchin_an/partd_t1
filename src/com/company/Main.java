package com.company;

public class Main {

    private final static int COUNT = 5;
    private static final String AUTHOR = "Грязный Билли";
    private final static int YEAR = 1500;

    public static void main(String[] args) {

        Book[] books = new Book[COUNT];
        BookGenerator bg = new BookGenerator();

        for (int i = 0; i < books.length; i++) {
            books[i] = new Book();
            books[i].setAuthor(bg.getAuthor());
            books[i].setTitle(bg.getTitle());
            books[i].setPublishing(bg.getYear());
        }
        int tmpYear = 0;
        for (Book book : books) {
            if (tmpYear < book.getPublishing()) {
                tmpYear = book.getPublishing();
            }
        }
        for (Book book : books) {
            if (book.getPublishing() == tmpYear) {
                System.out.println("The oldest book's author is " + book.getAuthor());
            }
        }
        for (Book book : books) {
            if (book.getAuthor().equals(AUTHOR)) {
                System.out.println("Billy's book " + book.getTitle());
            }
        }
        System.out.println("books before 1500");
        for (Book book : books) {
            if (book.getPublishing() < YEAR) {
                System.out.println(book.getAuthor());
                System.out.println(book.getTitle());
                System.out.println(book.getPublishing());
            }
        }
    }
}
